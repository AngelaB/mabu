package it.contact;

import it.contact.model.Contact;
import it.contact.service.ContactReader;
import it.contact.service.ContactWriter;

import java.util.List;

import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Runner {

	private static ClassPathXmlApplicationContext factory;
	private static List<Contact> contacts;
	
	public static void main(String[] args) {
		factory = new ClassPathXmlApplicationContext("factory.xml");
		// al metodo getBean passo l'interfaccia in modo che sia la factory a scegliere quale 
		// implementazione usare.
		ContactReader contactReader = factory.getBean(ContactReader.class);
		ContactWriter contactWriter = factory.getBean(ContactWriter.class);
		
		contacts = contactReader.readContacts();
		contactWriter.writeContact(contacts);
	}

}	
