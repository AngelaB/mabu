package it.contact.model;

public class Contact {
	
	private Integer idContact;
	private String firstname;
	private String lastname;
	
	public Integer getIdContact() {
		return idContact;
	}
	public void setIdContact(Integer idContact) {
		this.idContact = idContact;
	}
	public String getFirstname() {
		return firstname;
	}
	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}
	public String getLastname() {
		return lastname;
	}
	public void setLastname(String lastname) {
		this.lastname = lastname;
	}
	
}
