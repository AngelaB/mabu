package it.contact.service;

import it.contact.model.Contact;

import java.util.List;

public interface ContactWriter {
	
	void writeContact(List<Contact> contacts);

}
