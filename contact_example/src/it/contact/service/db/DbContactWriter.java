package it.contact.service.db;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import it.contact.dao.ContactDAO;
import it.contact.model.Contact;
import it.contact.service.ContactWriter;

public class DbContactWriter implements ContactWriter {
	
	@Autowired 
	private ContactDAO contactDAO;
	
	public void setContactDAO(ContactDAO contactDAO) {
		this.contactDAO = contactDAO;
	}

	@Override
	public void writeContact(List<Contact> contacts) {
		for(Contact contact : contacts){
			contactDAO.save(contact);
		}
	}

}
