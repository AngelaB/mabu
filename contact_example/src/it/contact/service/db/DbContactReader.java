package it.contact.service.db;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import it.contact.dao.ContactDAO;
import it.contact.model.Contact;
import it.contact.service.ContactReader;

public class DbContactReader implements ContactReader {
	
	@Autowired private ContactDAO contactDAO;
	
	public void setContactDAO(ContactDAO contactDAO) {
		this.contactDAO = contactDAO;
	}

	@Override
	public List<Contact> readContacts() {
		List<Contact> contacts = contactDAO.findAll();
		return contacts;
	}

}
