package it.contact.service;

import it.contact.model.Contact;

import java.util.List;

public interface ContactReader {
	
	List<Contact> readContacts();
}
