package it.contact.service.memory;

import it.contact.model.Contact;

import java.util.List;

public class InMemoryContactDelete {
	
	private List<Contact> contacts;
	private int id;

	public void setContacts(List<Contact> contacts) {
		this.contacts = contacts;
	}
	
	public void setId(int id) {
		this.id = id;
	}

	public void deleteContacts(){
		for (Contact contact : contacts){
			if(id == contact.getIdContact()){
				contacts.remove(contact);
			}
		}
	}

}
