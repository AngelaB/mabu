package it.contact.service.memory;

import it.contact.model.Contact;
import it.contact.service.ContactWriter;

import java.util.List;

public class InMemoryContactWriter implements ContactWriter {
	
	private List<Contact> contactsWrite;
	
	public void setContacts(List<Contact> contactsWrite) {
		this.contactsWrite = contactsWrite;
	}
	
	@Override
	public void writeContact(List<Contact> contacts) {
			contactsWrite.addAll(contacts);	
	}

}
