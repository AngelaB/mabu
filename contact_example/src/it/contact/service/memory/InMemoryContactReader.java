package it.contact.service.memory;

import it.contact.model.Contact;
import it.contact.service.ContactReader;

import java.util.ArrayList;
import java.util.List;

public class InMemoryContactReader implements ContactReader {

	private List<Contact> contacts = new ArrayList<Contact>();
	
	public void setContacts(List<Contact> contacts) {
		this.contacts = contacts;
	}
	
	// Injection tramite costruttore
//	public InMemoryContactReader(List<Contact> contacts) {
//		this.contacts = contacts;
//	}
	
	@Override
	public List<Contact> readContacts() {
		return contacts;
	}

}
