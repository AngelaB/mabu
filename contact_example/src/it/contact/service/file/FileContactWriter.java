package it.contact.service.file;

import it.contact.model.Contact;
import it.contact.service.ContactWriter;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

public class FileContactWriter implements ContactWriter {
	
	private FileWriter fileWriter;
	private BufferedWriter bufferWriter;
	
	public FileContactWriter(String fileName) throws IOException {
		fileWriter = new FileWriter(fileName);
	}
	
	@Override
	public void writeContact(List<Contact> contacts) {
		bufferWriter = new BufferedWriter(fileWriter);
		try {
			for(Contact contact : contacts){
				bufferWriter.write(contact.getIdContact()+ ";" +contact.getFirstname()+ ";" +contact.getLastname());
				bufferWriter.newLine();
			}
			bufferWriter.close();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {bufferWriter.close();} catch (IOException e) {}
		}
	}

}
