package it.contact.service.file;

import it.contact.model.Contact;
import it.contact.service.ContactReader;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class FileContactReader implements ContactReader {
	
	private InputStream inputStream = null;
	private InputStreamReader inputStreamReader;
	private BufferedReader reader;
	private Contact contact;
	private List<Contact> contacts = new ArrayList<Contact>();
	
	public FileContactReader(String fileName) throws FileNotFoundException{
		inputStream = new FileInputStream(fileName);
	}
	
	@Override
	public List<Contact> readContacts() {
		try {
			inputStreamReader = new InputStreamReader(inputStream);
			reader = new BufferedReader(inputStreamReader);
			
			String line = reader.readLine();
			while( line != null){
				contact = new Contact();
				String[] details = line.split(";");
				contact.setIdContact(Integer.parseInt( details[0] ));
				contact.setFirstname(details[1]);
				contact.setLastname(details[2]);
				contacts.add(contact);
				line = reader.readLine();
			}
			reader.close();
			
//			Scanner scanner = new Scanner(inputStream);
//			while (scanner.hasNext()) {
//				System.out.println(scanner.nextLine());
//			}
		} catch (IOException e) {
			e.printStackTrace();
		}finally {
			if (inputStream != null) {
				try {
					inputStream.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		return contacts;
	}

}
