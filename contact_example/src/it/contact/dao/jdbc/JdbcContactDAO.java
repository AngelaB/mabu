package it.contact.dao.jdbc;

import it.contact.dao.ContactDAO;
import it.contact.model.Contact;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

@Repository
public class JdbcContactDAO implements ContactDAO {
	
	private JdbcTemplate jdbcTemplate;
	
	@Autowired
    public void setDataSource(DataSource dataSource) {
        this.jdbcTemplate = new JdbcTemplate(dataSource);
    }
    
    private RowMapper<Contact> rowMapper = new RowMapper<Contact>() {
		@Override
    	public Contact mapRow(ResultSet rs, int idx) throws SQLException {
			Contact contact = new Contact();
			contact.setIdContact(rs.getInt("id_contact"));
			contact.setFirstname(rs.getString("firstname"));
			contact.setLastname(rs.getString("lastname"));
			return contact;
		}
	};

	@Override
	public List<Contact> findAll() {
		List<Contact> contacts = (List<Contact>) jdbcTemplate.query("select * from contact", rowMapper);                                                   
	    return contacts;  
	}

	@Override
	public void save(Contact contact) {	
		jdbcTemplate.update("insert into contact (id_contact, firstname, lastname) values (?, ?, ?)",
                new Object[] { contact.getIdContact(), contact.getFirstname(), contact.getLastname() }); 
	}
}
