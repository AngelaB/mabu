package it.contact.dao;

import it.contact.model.Contact;

import java.util.List;

public interface ContactDAO {

	public List<Contact> findAll();
	
	public void save(Contact contact);
	
}
