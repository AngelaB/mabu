package it.contact.test;

import it.contact.model.Contact;
import it.contact.service.memory.InMemoryContactWriter;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;

public class InMemoryContactWriterTest {
	
	private InMemoryContactWriter writer;
	private Contact contact;
	
	@Test
	public void writeContact(){
		// 1-Preparazione
		
		writer = new InMemoryContactWriter();
		contact = new Contact();
		List<Contact> contacts = new ArrayList<Contact>();
		List<Contact> contactsWrite = new ArrayList<Contact>();
		
		contact.setFirstname("Angela");
		contact.setLastname("Busato");
		contacts.add(contact);
		
		writer.setContacts(contactsWrite);
		
		// 2-Azione
		
		writer.writeContact(contacts);
		
		// 3-Verifica
		
		Assert.assertEquals(contacts, contactsWrite);
		
		
	}

}
