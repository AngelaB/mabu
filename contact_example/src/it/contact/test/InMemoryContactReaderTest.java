package it.contact.test;

import it.contact.model.Contact;
import it.contact.service.memory.InMemoryContactReader;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;

public class InMemoryContactReaderTest {
	
	private InMemoryContactReader reader;
	private Contact contact;
	
	@Test
	public void readContacts(){
		// 1-Preparazione
		reader = new InMemoryContactReader();
		List<Contact> contacts = new ArrayList<Contact>();
		List<Contact> contactsTest = new ArrayList<Contact>();
		contact = new Contact();
		
		contact.setFirstname("Angela");
		contact.setLastname("Busato");
		contacts.add(contact);
		reader.setContacts(contacts);
		
		// 2-Azione
		
		contactsTest = reader.readContacts();
		
		// 3-Verifica
		
		Assert.assertNotNull(contactsTest);
		Assert.assertEquals(1,contactsTest.size());
		
		
	}

}
